const tabs = document.querySelectorAll('.tabs-title')
const tabText = document.querySelectorAll('.tabsItem')


tabs.forEach(function(item) {
    item.addEventListener('click', function() {
        let activeTab = item;
        let tabId = activeTab.getAttribute('data-tab');
        let currentText = document.querySelector(tabId)

        tabs.forEach(function(item) {
            item.classList.remove('active')            
        });
        
        tabText.forEach(function(item) {
            item.classList.remove('active')            
        });

        currentText.classList.add('active')
        activeTab.classList.add('active')
    })
})